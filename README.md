# GX304_ProcGen

## Cartography Generator

A simple noise based terrain generation tool that can be used to generate single texture maps. This tool comes with some basic Scriptable Object presets but is otherwise entirely customisable.

## Installation

Download the Asset Pack under __CartographyGenerator to include this tool in your projects.

## Requirements

EasyButtons - Used to make generating maps easier.

Unity Recorder - Not necessary but makes saving maps as external images easier.

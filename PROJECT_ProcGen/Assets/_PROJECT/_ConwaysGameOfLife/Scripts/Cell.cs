using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cell : MonoBehaviour
{
    [SerializeField] SpriteRenderer spriteRenderer;

    [SerializeField] Color colourAlive;
    [SerializeField] Color colourDead;

    [Header("Debug")]
    public Vector2Int coordinate;

    public bool isAlive = false;

    public List<Cell> neighbourhood = new List<Cell>();

    internal void SetState(bool state)
    {
        if (isAlive != state) Flip();
    }

    void Flip()
    {
        isAlive = !isAlive;

        spriteRenderer.color = isAlive ? colourAlive : colourDead;
    }

    internal void SetCoordinate(Vector2Int coord)
    {
        coordinate = coord;
    }

    internal void SetNeighbourhood(List<Cell> _neighbourhood)
    {
        neighbourhood = _neighbourhood;
    }

    internal void SetNextGeneration(bool alive)
    {
        StartCoroutine(ApplyNextGeneration(alive));
    }

    IEnumerator ApplyNextGeneration(bool state)
    {
        yield return new WaitForEndOfFrame();
        SetState(state);
    }
}

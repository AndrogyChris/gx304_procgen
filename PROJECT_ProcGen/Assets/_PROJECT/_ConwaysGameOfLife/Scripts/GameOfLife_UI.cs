using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameOfLife_UI : MonoBehaviour
{
    public void StartSimulation()
    {
        ConwayGameOfLife.Instance.StartSimulation();
    }

    public void StopSimulation()
    {
        ConwayGameOfLife.Instance.StopSimulation();
    }

    public void ResetSimulation()
    {
        ConwayGameOfLife.Instance.ResetSimulation();
    }

    public void SetWaitTime(float time)
    {
        ConwayGameOfLife.Instance.SetWaitTime(time);
    }
}

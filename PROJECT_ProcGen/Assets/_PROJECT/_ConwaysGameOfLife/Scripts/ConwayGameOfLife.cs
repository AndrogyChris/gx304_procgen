using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class ConwayGameOfLife : MonoBehaviour
{
    public static ConwayGameOfLife Instance;

    [SerializeField] InputActionReference selectAction;
    [SerializeField] InputActionReference mousePositionAction;

    Vector2 mousePosition;

    bool selectingCells = false;
    bool simulating = false;

    float waitTime;

    Camera cam;

    private void Awake()
    {
        Instance = this;
    }

    private void Start()
    {
        cam = Camera.main;

        selectAction.action.Enable();
        selectAction.action.started += x =>
        {
            StartCoroutine(SelectingCells());
        };
        selectAction.action.canceled += x =>
        {
            selectingCells = false;
        };

        mousePositionAction.action.Enable();
        mousePositionAction.action.performed += x =>
        {
            mousePosition = x.ReadValue<Vector2>();
        };
    }

    IEnumerator SelectingCells()
    {
        selectingCells = true;

        bool firstSelect = true;
        bool targetState = false;

        while (selectingCells)
        {
            Vector3 worldPoint = cam.ScreenToWorldPoint(mousePosition);
            worldPoint = GridGenerator.Instance.transform.InverseTransformPoint(worldPoint);
            worldPoint.SnapPosition();

            if (worldPoint.x > GridGenerator.Instance.gridSize.x || worldPoint.x < 0) goto skip;
            if (worldPoint.y > GridGenerator.Instance.gridSize.y || worldPoint.y < 0) goto skip;

            int childIndex = (int)worldPoint.x + ((int)worldPoint.y * GridGenerator.Instance.gridSize.y);

            if (firstSelect)
            {
                targetState = !GridGenerator.Instance.cells[childIndex].isAlive;
                firstSelect = false;
            }

            GridGenerator.Instance.cells[childIndex].SetState(targetState);

            skip:
            yield return null;
        }
    }

    public void StartSimulation()
    {
        StartCoroutine(GameOfLife());
    }

    public void StopSimulation()
    {
        simulating = false;
    }

    public void ResetSimulation()
    {
        for (int i = 0; i < GridGenerator.Instance.cells.Count; i++)
        {
            GridGenerator.Instance.cells[i].SetState(false);
        }
    }

    public void SetWaitTime(float time)
    {
        waitTime = time;
    }

    IEnumerator GameOfLife()
    {
        simulating = true;
        while (simulating)
        {
            for (int i = 0; i < GridGenerator.Instance.cells.Count; i++)
            {
                Cell cell = GridGenerator.Instance.cells[i];
                int liveNeighbours = 0;
                for (int k = 0; k < cell.neighbourhood.Count; k++)
                {
                    if (cell.neighbourhood[k].isAlive) liveNeighbours++;
                }

                if (cell.isAlive && liveNeighbours < 2)
                {
                    cell.SetNextGeneration(false);
                }

                if (cell.isAlive && (liveNeighbours == 2 || liveNeighbours == 3))
                {
                    cell.SetNextGeneration(true);
                }

                if (cell.isAlive && liveNeighbours > 3)
                {
                    cell.SetNextGeneration(false);
                }

                if (!cell.isAlive && liveNeighbours == 3)
                {
                    cell.SetNextGeneration(true);
                }
            }

            yield return new WaitForSeconds(waitTime);
        }
    }
}

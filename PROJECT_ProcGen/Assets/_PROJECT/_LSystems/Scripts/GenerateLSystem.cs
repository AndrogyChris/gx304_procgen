using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class GenerateLSystem : MonoBehaviour
{
    [Header("Turt")]
    [SerializeField] Transform turtform;
    [SerializeField] LineRendererObjectPool turtPond;

    Camera cam;

    [Header("L-SYstem")]
    [SerializeField] string axiom;
    [SerializeField] List<Grammar> grammars = new List<Grammar>();

    [Header("Settings")]
    [SerializeField] int generations = 3;
    [SerializeField] float lineLength = 0.5f;
    [SerializeField] float rotationAngle = 5f;
    [SerializeField, Range(0, 10)] float frameWait = 0;
    [SerializeField, Range(0, 100)] int stepsPerFrame = 1; 
    

    [Header("Output")]
    [SerializeField, TextArea(3, 10)] string sequence;
    [SerializeField] bool drawingSequence = false;

    private void Start()
    {
        cam = Camera.main;
    }

    [EasyButtons.Button]
    public void Generate()
    {
        drawingSequence = true;
        StartCoroutine(GenerateSequence(() =>
        {
            StartCoroutine(DrawSeqence(sequence));
        }));
    }

    [EasyButtons.Button]
    public void StopGenerate()
    {
        drawingSequence = false;
    }

    IEnumerator GenerateSequence(UnityAction OnComplete)
    {
        sequence = axiom;

        for (int i = 0; i < generations; i++)
        {
            sequence = ProcessSequence(sequence);

            //yield return new WaitForSeconds(1);
        }

        yield return null;
        OnComplete.Invoke();
    }

    private string ProcessSequence(string sequence)
    {
        string processedSequence = string.Empty;

        char[] characters = sequence.ToCharArray();

        for (int i = 0; i < characters.Length; i++)
        {
            bool found = false;
            for (int g = 0; g < grammars.Count; g++)
            {
                if (characters[i] == grammars[g].variable)
                {
                    processedSequence += grammars[g].rule;

                    found = true;
                    break;
                }
            }

            if (!found)
            {
                processedSequence += characters[i];
            }
        }

        return processedSequence;
    }

    IEnumerator DrawSeqence(string sequence)
    {
        int stepCount = 0;

        char[] characters = sequence.ToCharArray();

        turtform.localPosition = Vector3.zero;
        turtform.localEulerAngles = Vector3.zero;

        List<LSystemLine> lines = new List<LSystemLine>();
        LSystemLine line = turtPond.GetFromObjectPool();
        lines.Add(line);

        Stack<PosRot> posrotMemory = new Stack<PosRot>();

        for (int i = 0; i < characters.Length; i++)
        {
            //LSystem_Koch(characters[i]);
            BinaryTree(characters[i], ref posrotMemory, ref lines);

            line = lines[lines.Count - 1];
            line.lineRenderer.positionCount++;
            line.lineRenderer.SetPosition(line.lineRenderer.positionCount - 1, turtform.localPosition);

            if (turtform.localPosition.y > cam.orthographicSize * 2)
            {
                cam.orthographicSize = ((int)turtform.localPosition.y / 2) + 1;
                transform.position = new Vector3(0, -cam.orthographicSize, 0);
            }

            stepCount++;
            if (stepCount >= stepsPerFrame)
            {
                stepCount = 0;
                yield return new WaitForSeconds(frameWait);
            }

            if (!drawingSequence)
                break;
        }

        while (drawingSequence)
            yield return null;
        
        if (!drawingSequence && lines.Count > 0)
        {
            for (int i = 0; i < lines.Count; i++)
            {
                turtPond.PutIntoObjectPool(lines[i]);
            }

            cam.orthographicSize = 10;
            transform.position = -Vector3.up * (cam.orthographicSize / 2);
        }

        yield return null;
    }

    private void LSystem_Koch(char _char)
    {
        switch (_char)
        {
            case 'F'://Forward
                Debug.Log($"ONWARD!");
                turtform.Translate(Vector3.up * lineLength);
                break;
            case '+'://Turn Left
                Debug.Log($"LEFT!");
                turtform.Rotate(Vector3.forward * -rotationAngle);
                break;
            case '-'://Turn Right
            case '?':
                Debug.Log($"RIGHT!");
                turtform.Rotate(Vector3.forward * rotationAngle);
                break;
            default:
                Debug.Log($"Invalid Char {_char}");
                break;
        }
    }

    private void BinaryTree(char _char, ref Stack<PosRot> posrotMemory, ref List<LSystemLine> lines)
    {
        switch (_char)
        {
            case '1':
            case '0'://Forward
                Debug.Log($"ONWARD!");
                turtform.Translate(Vector3.up * lineLength);
                break;
            case '['://Turn Left
                Debug.Log($"SAVE! LEFT!");
                posrotMemory.Push(new PosRot(turtform.localPosition, turtform.rotation));
                turtform.Rotate(Vector3.forward * -rotationAngle);
                break;
            case ']'://Turn Right
                Debug.Log($"SAVE! RIGHT!");
                PosRot cachedPosRot = posrotMemory.Pop();
                turtform.localPosition = cachedPosRot.position;
                turtform.rotation = cachedPosRot.rotation;

                LSystemLine line = turtPond.GetFromObjectPool();
                lines.Add(line);

                turtform.Rotate(Vector3.forward * rotationAngle);
                break;
            default:
                Debug.Log($"Invalid Char {_char}");
                break;
        }
    }
}

[System.Serializable]
public class Grammar
{
    public char variable;
    public string rule;
}

[System.Serializable]
public struct PosRot
{
    public Vector3 position;
    public Quaternion rotation;

    public PosRot(Vector3 position, Quaternion rotation)
    {
        this.position = position;
        this.rotation = rotation;
    }
}

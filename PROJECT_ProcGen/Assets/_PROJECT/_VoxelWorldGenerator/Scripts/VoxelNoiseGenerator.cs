using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class VoxelNoiseGenerator : MonoBehaviour
{
    [SerializeField] RawImage noiseOutput;
    [SerializeField] int textureSize;
    [SerializeField] int terrainSize;
    [SerializeField] int maxHeight;
    [SerializeField] float noiseScale;
    [SerializeField] int seed;
    [SerializeField] bool randomiseSeed;

    Texture2D noiseTexture;

    [SerializeField] ObjectPool_VoxelBlock grassPool;
    [SerializeField] ObjectPool_VoxelBlock dirtPool;

    List<VoxelBlock> grassBlocks = new List<VoxelBlock>();
    List<VoxelBlock> dirtBlocks = new List<VoxelBlock>();

    public void GenerateNoise()
    {
        noiseTexture = new Texture2D(textureSize, textureSize);
        float pixelValue = 0;

        if (randomiseSeed) seed = Random.Range(0, int.MaxValue);

        System.Random rng = new System.Random(seed);

        float offset = (float)rng.NextDouble() * rng.Next(100);

        for (int h = 0; h < textureSize; h++)
        {
            for (int w = 0; w < textureSize; w++)
            {
                float xCoord = offset + ((float)w / (float)textureSize) * noiseScale;
                float yCoord = offset + ((float)h / (float)textureSize) * noiseScale;

                pixelValue = Mathf.PerlinNoise(xCoord, yCoord);

                noiseTexture.SetPixel(w, h, new Color(pixelValue, pixelValue, pixelValue));
            }
        }

        noiseTexture.Apply();
        noiseOutput.texture = noiseTexture;
    }

    public void GenerateVoxels()
    {
        ClearVoxels();

        for (int w = 0; w < terrainSize; w++)
        {
            for (int h = 0; h < terrainSize; h++)
            {
                int xCoord = Mathf.FloorToInt(((float)w * (float)textureSize) / (float)terrainSize);
                int yCoord = Mathf.FloorToInt(((float)h * (float)textureSize) / (float)terrainSize);

                float pixelValue = noiseTexture.GetPixel(xCoord, yCoord).r;
                float blockHeight = pixelValue * maxHeight;
                blockHeight = Mathf.Round(blockHeight);

                VoxelBlock newBlock = grassPool.GetFromObjectPool();
                newBlock.transform.position = new Vector3(w, blockHeight, h);
                grassBlocks.Add(newBlock);

                if (blockHeight > 0)
                {
                    for (int i = 0; i < blockHeight; i++)
                    {
                        newBlock = dirtPool.GetFromObjectPool();
                        newBlock.transform.position = new Vector3(w, i, h);
                        dirtBlocks.Add(newBlock);
                    }
                }
            }
        }
    }

    void ClearVoxels()
    {
        for (int i = 0; i < grassBlocks.Count; i++)
        {
            grassPool.PutIntoObjectPool(grassBlocks[i]);
        }
        grassBlocks.Clear();

        for (int i = 0; i < dirtBlocks.Count; i++)
        {
            dirtPool.PutIntoObjectPool(dirtBlocks[i]);
        }
        dirtBlocks.Clear();
    }
}

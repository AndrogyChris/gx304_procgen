using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BSPDungeonRoom : MonoBehaviour
{
    [SerializeField] GameObject finalRoom;
    internal void GenerateFinalRoom()
    {
        finalRoom.SetActive(true);
        finalRoom.transform.localPosition += new Vector3()
        {
            x = Random.Range(-0.25f, 0.25f),
            y = 0f,
            z = Random.Range(-0.25f, 0.25f)
        };
    }

    private void OnDisable()
    {
        finalRoom.SetActive(false);
    }
}

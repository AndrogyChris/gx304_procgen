using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Probability : MonoBehaviour
{
    [SerializeField] List<EnemyProbabilityValue> enemyProbabilityValues = new List<EnemyProbabilityValue>();

    [Header("Debug")]
    [SerializeField] float totalProbability;

    public void RollNumber(int numberOfRolls)
    {
        enemyProbabilityValues.ForEach(x => x.rollCount = 0);
        enemyProbabilityValues.ForEach(x => x.probabilityIncrement = 0);

        for (int i = 0; i < numberOfRolls; i++)
        {
            RollNumber();
        }
    }

    public void RollNumber()
    {
        totalProbability = 0;

        enemyProbabilityValues.ForEach(x =>
        {
            totalProbability += x.probability + x.probabilityLiklihoodOffset;
        });

        float randomNum = Random.Range(0, totalProbability);

        EnemyProbabilityValue selectedValue = null;
        float runningTotal = 0;
        bool foundCandidate = false;

        for (int i = 0; i < enemyProbabilityValues.Count; i++)
        {
            bool offsetvalue = true;
            if (!foundCandidate)
            {
                runningTotal += enemyProbabilityValues[i].probability + enemyProbabilityValues[i].probabilityLiklihoodOffset;

                if (randomNum <= runningTotal)
                {
                    selectedValue = enemyProbabilityValues[i];
                    foundCandidate = true;
                    offsetvalue = false;

                    enemyProbabilityValues[i].probabilityLiklihoodOffset = 0;
                    enemyProbabilityValues[i].rollCount++;

                    string color = selectedValue.enemyDifficulty == EnemyDifficulty.Easy ? "green" :
                        selectedValue.enemyDifficulty == EnemyDifficulty.Medium ? "yellow" : "red";

                    Debug.Log($"Rolled {randomNum}/{runningTotal} and found <color='{color}'>{selectedValue.enemyDifficulty}</color> enemy");
                }
            }

            if (offsetvalue)
            {
                enemyProbabilityValues[i].probabilityLiklihoodOffset += enemyProbabilityValues[i].probabilityIncrement;
            }
        }
    }
}

public enum EnemyDifficulty
{
    Easy,
    Medium,
    Hard
}

[System.Serializable]
public class EnemyProbabilityValue
{
    public EnemyDifficulty enemyDifficulty;
    public float probability;
    public float probabilityIncrement = 1f;

    public float probabilityLiklihoodOffset;
    public int rollCount;
}

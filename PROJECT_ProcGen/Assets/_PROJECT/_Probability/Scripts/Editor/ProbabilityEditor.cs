using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(Probability))]
public class ProbabilityEditor : Editor
{
    Probability probability;

    private void OnEnable()
    {
        probability = (Probability)target;
    }

    public override void OnInspectorGUI()
    {
        serializedObject.Update();

        EditorGUILayout.BeginHorizontal(EditorStyles.helpBox);
        {
            if (GUILayout.Button("Roll 1"))
            {
                probability.RollNumber();
            }
            if (GUILayout.Button("Roll 500"))
            {
                probability.RollNumber(500);
            }
        }
        EditorGUILayout.EndHorizontal();

        SerializedProperty enemyprobs = serializedObject.FindProperty("enemyProbabilityValues");
        EditorGUILayout.PropertyField(enemyprobs, GUIContent.none);

        serializedObject.ApplyModifiedProperties();
    }
}

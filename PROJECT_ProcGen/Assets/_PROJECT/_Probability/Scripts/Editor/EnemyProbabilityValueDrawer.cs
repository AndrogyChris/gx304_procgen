using UnityEngine;
using UnityEditor;

[CustomPropertyDrawer(typeof(EnemyProbabilityValue))]
public class EnemyProbabilityValueDrawer : PropertyDrawer
{
    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        EditorGUILayout.BeginVertical(EditorStyles.helpBox);
        {
            SerializedProperty enemyDifficulty = property.FindPropertyRelative("enemyDifficulty");
            EditorGUILayout.PropertyField(enemyDifficulty);

            EditorGUILayout.BeginHorizontal();
            {
                EditorGUILayout.BeginVertical();
                {
                    GUILayout.Label("Probablitlity", EditorStyles.miniLabel);
                    SerializedProperty probability = property.FindPropertyRelative("probability");
                    EditorGUILayout.PropertyField(probability, GUIContent.none);
                }
                EditorGUILayout.EndVertical();

                EditorGUILayout.BeginVertical();
                {
                    GUILayout.Label("Probablitlity Increment", EditorStyles.miniLabel);
                    SerializedProperty probabilityIncrement = property.FindPropertyRelative("probabilityIncrement");
                    EditorGUILayout.PropertyField(probabilityIncrement, GUIContent.none);
                }
                EditorGUILayout.EndVertical();
            }
            EditorGUILayout.EndHorizontal();

            EditorGUILayout.BeginHorizontal();
            {
                EditorGUILayout.BeginHorizontal();
                {
                    GUILayout.Label("Probablitlity Offset", EditorStyles.miniLabel);
                    SerializedProperty probabilityLiklihoodOffset = property.FindPropertyRelative("probabilityLiklihoodOffset");
                    GUILayout.Label(probabilityLiklihoodOffset.floatValue.ToString(), EditorStyles.miniLabel);
                }
                EditorGUILayout.EndHorizontal();

                EditorGUILayout.BeginHorizontal();
                {
                    GUILayout.Label("Total Roll Count", EditorStyles.miniLabel);
                    SerializedProperty rollCount = property.FindPropertyRelative("rollCount");
                    GUILayout.Label(rollCount.intValue.ToString(), EditorStyles.miniLabel);
                }
                EditorGUILayout.EndHorizontal();
            }
            EditorGUILayout.EndHorizontal();
        }
        EditorGUILayout.EndVertical();
        EditorGUILayout.Space();
    }
}

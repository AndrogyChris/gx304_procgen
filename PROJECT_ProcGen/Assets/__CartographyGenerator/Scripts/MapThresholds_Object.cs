using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "MapThreshold_", menuName = "Data/Map Threshold Data", order = 0)]
public class MapThresholds_Object : ScriptableObject
{
    public string mapThresholdsName;

    public List<Thresholds> mapThresholds = new List<Thresholds>();
}

[System.Serializable]
public class Thresholds
{
    public string name;
    [Range(0,1)]
    public float threshold;
    public Color colour;
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "NoiseData_", menuName = "Data/Noise Data", order = 0)]
public class NoiseData_Object : ScriptableObject
{
    public string noiseDataName;

    [Header("Noise Settings")]
    public float noiseScaleData;
    public int octavesData;
    public float persistanceData;
    public float lacunarityData;
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CartographyNoiseGenerator : MonoBehaviour
{
    [Header("Settings")]
    [SerializeField] RawImage landmassNoise;
    [SerializeField] Vector2Int textureSize;

    [Header("Seeding")]
    [SerializeField] int seed;
    [SerializeField] bool randomiseSeed;
    [SerializeField] int maxStoredSeeds = 10;
    [SerializeField] List<int> storedSeeds = new List<int>();
    int currentSeedIndex;

    [Header("Noise")]
    [SerializeField] NoiseData_Object noiseData;
    [SerializeField] float noiseScale;
    [SerializeField] int octaves;
    [SerializeField] float persistance;
    [SerializeField] float lacunarity;

    [Header("Colours")]
    [SerializeField] MapThresholds_Object thresholdObject;

    [Header("Additional Passes")]
    [SerializeField] List<AdditionalPass_Object> additionalPasses = new List<AdditionalPass_Object>();

    Texture2D noiseTexture;

    [EasyButtons.Button]
    public void Generate()
    {
        noiseTexture = new Texture2D(textureSize.x, textureSize.y);
        GetSeedAndNoiseData();
        GenerateLandmassNoise();
        RunAdditionalPasses();
    }

    public void GetSeedAndNoiseData()
    {
        if (randomiseSeed)
            seed = Random.Range(0, int.MaxValue);

        if (storedSeeds.Count == 0)
        {
            currentSeedIndex = 0;
            storedSeeds.Add(seed);
        }
        else if (seed != storedSeeds[0])
        {
            currentSeedIndex = 0;
            storedSeeds.Insert(0, seed);

            if (storedSeeds.Count >= maxStoredSeeds)
            {
                for (int i = storedSeeds.Count - 1; i > maxStoredSeeds - 1; i--)
                {
                    storedSeeds.Remove(storedSeeds[i]);
                }
            }
        }

        if (noiseData != null)
        {
            noiseScale = noiseData.noiseScaleData;
            octaves = noiseData.octavesData;
            persistance = noiseData.persistanceData;
            lacunarity = noiseData.lacunarityData;
        }
    }

    public void GenerateLandmassNoise()
    {
        Debug.Log("Generating Base Landmass");

        float[,] noiseMap = new float[textureSize.x, textureSize.y];

        System.Random rng = new System.Random(seed);

        float maxNoiseHeight = float.MinValue;
        float minNoiseHeight = float.MaxValue;

        float offset = (float)rng.NextDouble() * rng.Next(100);

        for (int h = 0; h < textureSize.y; h++)
        {
            for (int w = 0; w < textureSize.x; w++)
            {
                float amplitude = 1;
                float frequency = 1;
                float noiseHeight = 0;

                for (int i = 0; i < octaves; i++)
                {
                    float xCoord = offset + ((float)w / (float)textureSize.x) * noiseScale * frequency;
                    float yCoord = offset + ((float)h / (float)textureSize.y) * noiseScale * frequency;

                    float pixelValue = Mathf.PerlinNoise(xCoord, yCoord) * 2 - 1;

                    noiseHeight += pixelValue * amplitude;

                    amplitude *= persistance;
                    frequency *= lacunarity;
                }

                if (noiseHeight > maxNoiseHeight)
                {
                    maxNoiseHeight = noiseHeight;
                }
                else if (noiseHeight < minNoiseHeight)
                {
                    minNoiseHeight = noiseHeight;
                }
                noiseMap[w, h] = noiseHeight;
            }
        }

        for (int h = 0; h < textureSize.y; h++)
        {
            for (int w = 0; w < textureSize.x; w++)
            {
                float inversed = Mathf.InverseLerp(minNoiseHeight, maxNoiseHeight, noiseMap[w, h]);
                noiseMap[w, h] = inversed;

                for (int i = 0; i < thresholdObject.mapThresholds.Count; i++)
                {
                    if (noiseMap[w, h] <= thresholdObject.mapThresholds[i].threshold)
                    {
                        noiseTexture.SetPixel(w, h, thresholdObject.mapThresholds[i].colour);
                        break;
                    }
                }
            }
        }

        noiseTexture.Apply();
        landmassNoise.texture = noiseTexture;
    }

    public void RunAdditionalPasses()
    {
        for (int p = 0; p < additionalPasses.Count; p++)
        {
            Debug.Log($"Generating Pass {p}");

            float _noiseScale = additionalPasses[p].passNoiseData.noiseScaleData;
            float _octaves = additionalPasses[p].passNoiseData.octavesData;
            float _persistance = additionalPasses[p].passNoiseData.persistanceData;
            float _lacunarity = additionalPasses[p].passNoiseData.lacunarityData;

            float[,] _noiseMap = new float[textureSize.x, textureSize.y];

            System.Random rng = new System.Random(seed);

            float maxNoiseHeight = float.MinValue;
            float minNoiseHeight = float.MaxValue;

            float offset = (float)rng.NextDouble() * rng.Next(100 * (p + 1));

            AdditionalPass_Object pass = additionalPasses[p];

            for (int h = 0; h < textureSize.y; h++)
            {
                for (int w = 0; w < textureSize.x; w++)
                {
                    float amplitude = 1;
                    float frequency = 1;
                    float noiseHeight = 0;

                    for (int i = 0; i < _octaves; i++)
                    {
                        float xCoord = offset + ((float)w / (float)textureSize.x) * _noiseScale * frequency;
                        float yCoord = offset + ((float)h / (float)textureSize.y) * _noiseScale * frequency;

                        float pixelValue = Mathf.PerlinNoise(xCoord, yCoord) * 2 - 1;

                        noiseHeight += pixelValue * amplitude;

                        amplitude *= _persistance;
                        frequency *= _lacunarity;
                    }

                    if (noiseHeight > maxNoiseHeight)
                    {
                        maxNoiseHeight = noiseHeight;
                    }
                    else if (noiseHeight < minNoiseHeight)
                    {
                        minNoiseHeight = noiseHeight;
                    }
                    _noiseMap[w, h] = noiseHeight;
                }
            }

            for (int h = 0; h < textureSize.y; h++)
            {
                for (int w = 0; w < textureSize.x; w++)
                {
                    float inversed = Mathf.InverseLerp(minNoiseHeight, maxNoiseHeight, _noiseMap[w, h]);
                    _noiseMap[w, h] = inversed;

                    switch (pass.thresholdApplicationDirection)
                    {
                        case ThresholdApplications.above:
                            if (_noiseMap[w, h] < pass.passThreshold)
                                continue;
                            else
                                break;
                        case ThresholdApplications.below:
                            if (_noiseMap[w, h] >= pass.passThreshold)
                                continue;
                            else
                                break;
                        case ThresholdApplications.aboveAndBelow:
                            break;
                        default:
                            break;
                    }

                    for (int i = 0; i < pass.passColourThresholdData.mapThresholds.Count; i++)
                    {
                        if (_noiseMap[w, h] <= pass.passColourThresholdData.mapThresholds[i].threshold)
                        {
                            noiseTexture.SetPixel(w, h, pass.passColourThresholdData.mapThresholds[i].colour);
                            break;
                        }
                    }
                }
            }

            noiseTexture.Apply();
            landmassNoise.texture = noiseTexture;
        }
    }

    /*public void CutAwayLandmass()
    {
        Debug.Log("Cutting Away at Landmass");

        System.Random rng = new System.Random(seed);
        float offset = (float)rng.NextDouble() * rng.Next(200);

        for (int h = 0; h < textureSize.y; h++)
        {
            for (int w = 0; w < textureSize.x; w++)
            {
                float xCoord = offset + ((float)w / (float)textureSize.x) * noiseScale * 5;
                float yCoord = offset + ((float)h / (float)textureSize.y) * noiseScale * 5;


                float pixelValue = Mathf.PerlinNoise(xCoord, yCoord);
                Color32 pixelColour = noiseTexture.GetPixel(w, h);

                if ((Color)pixelColour != oceanColour)
                {
                    if (pixelValue <= landmassThreshold / 2)
                    {
                        noiseTexture.SetPixel(w, h, oceanColour);
                    }
                }
            }
        }

        noiseTexture.Apply();
        landmassNoise.texture = noiseTexture;
    }*/
}

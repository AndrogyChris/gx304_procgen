using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "AdditionalPassData_", menuName = "Data/Additional Pass Data", order = 0)]
public class AdditionalPass_Object : ScriptableObject
{
    public float passThreshold;
    public ThresholdApplications thresholdApplicationDirection;

    public NoiseData_Object passNoiseData;
    public MapThresholds_Object passColourThresholdData;
}

public enum ThresholdApplications
{
    above,
    below,
    aboveAndBelow
}
